---
layout: page
title:  "About PI-Rail"
---
 This is the technical documentation project of [PI-Rail](https://gitlab.com/pi-rail).
 
 User documentation (currently only in german) can be found on [CTC-System Docu Site](https://ctc-system.gitlab.io/ctc-doku/).