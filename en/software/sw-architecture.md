---
layout: page
title:  "Software Architecture"
tags: Software Architecture
---

## Overview

Following UML deployment diagram shows the artifacts of PI-Rail from source code to binary distribution: 

![UML Artifacts Overview]({{site.baseurl}}/assets/images/UML-Artifacts.png)

## App components

The following UML component diagram show the main components of PI-Rail app and their relation to the three main platforms:
 
![UML Components App]({{site.baseurl}}/assets/images/UML-App.png)

## Firmware components

The following UML component diagram shows the firmware components and used libraries:

![UML Components Firmware]({{site.baseurl}}/assets/images/UML-Firmware.png)
