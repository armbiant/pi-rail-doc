---
layout: page
title:  "Migration PI-Rail v2.xx auf v3.xx"
tags: Software Architecture Upgrade
---
## Step 1: Module Firmware upgrade

1. Starten Sie die neue CTC-App (v3.xx)
1. Öffnen sie den "Konfigurator" und aktualisieren Sie die Firmware aller CTC-Module.
                     
**Führen Sie die folgenden Schritte je Modul komplett aus, sonst kann es mühsam werden das jeweilige Modul zu identifizieren.** 

## Step 2: netCfg.xml aktualisieren

![Device Error]({{site.baseurl}}/assets/images/Device-Error.png)
                                        
Beispiel für eine korrekte netCfg.xml:
```
<?xml version="1.0" encoding="UTF-8" ?>
<netCfg xmlns="http://res.pirail.org/pi-rail.xsd">
  <wifiCfg ssid="PI-Rail" pass="AbS=1gPw"/>
</netCfg>
```

1. Öffnen Sie die neue CTC-App (v3.xx)
1. Die aktualisierten Module sollten im Konfigurator als "Unknown Device" angezeigt werden.
1. Klicken Sie auf den "Edit"-Button --> es öffnet sich der Dialog "Device initialisieren".
1. Dort sollte bei "Letztes Datenpaket" folgendes stehen: " : ERROR - fileCfg is missing or invalid"
1. Klicken Sie auf "HTML-Seite" --> die WebSite des Moduls erscheint im Internet-Browser
1. Fügen Sie falls in er netCfg.xml noch nicht vorhanden folgendes als erste Zeile ein: ```<?xml version="1.0" encoding="UTF-8" ?>```    
1. Ersetzen Sie in einer der ersten Zeilen der netCfg.xml den Namespace (```xmlns="pi-rail-v1"```) durch ```xmlns="http://res.pirail.org/pi-rail.xsd"```
1. Falls noch eine Zeile existiert, die mit ```  <msgCfg ``` beginnt, dann löschen Sie diese.    
1. Klicken Sie auf "Save" --> es erschient OK und das Modul startet neu

## Step 2: fileCfg.xml ersetzen

1. Gehen Sie zurück zur index-Seite des Moduls
2. Klicken Sie auf "Upload file"
3. Wählen Sie aus der neuen CTC-App im Ordner config die Datei "fileCfg.xml" aus
4. Klicken Sie auf "Upload" --> es erschient OK und das Modul startet neu

## Step 3: ioCfg.xml ersetzen

**ACHTUNG:** Wird in diesem Schritt die falsche ioCfg.xml hochgeladen kann sowohl das Modul als auch daran angeschlossene Hardware dauerhaft beschädigt werden.

1. Gehen Sie zurück zur index-Seite des Moduls
2. Klicken Sie auf "Upload file"
3. Wählen Sie aus der neuen CTC-App im Ordner config die **zum Board passende** "ioCfg.xml" aus.
4. Klicken Sie auf "Upload" --> es erschient OK und das Modul startet neu

## Step 4: cfg.xml ersetzen
                         
Für Weichen und IO-Boards:

1. Gehen Sie zurück zur index-Seite des Moduls
1. **Werfen Sie ggf. noch einen Blick in die alte cfg.xml und merken Sie sich die Namen von Modul und Weiche (toCfg), Signal (siCfg).** 
1. Klicken Sie auf "Upload file"
1. Wählen Sie aus der neuen CTC-App im Ordner config die Datei "cfg.xml" aus - sie enthält eine leere Konfiguration.
1. Klicken Sie auf "Upload" --> es erschient OK und das Modul startet neu

Für Lokomotiven lohnt es sich die Daten des Motors zu retten:

1. Gehen Sie zurück zur index-Seite des Moduls
1. Öffnen Sie die alte cfg.xml und kopieren Sie deren Text in einen XML-Editor.
1. Wählen Sie aus der neuen CTC-App unterhalb des Ordners config die zu CTC-Modul und Loktyp passende "cfg.xml".
1. Fügen Sie in diese "cfg.cml" die Motordaten der alten Lok ein und ersetzen die Tags "moMode" durch "mode".
1. Löschen Sie aus den Tags "mode" die nicht mehr gültigen Attribute "min" und "max".
1. Kopieren Sie den Text der geänderten "cfg.xml" in den Editor-Bereich der HTML-Seite.    
1. Klicken Sie auf "Save" --> es erschient OK und das Modul startet neu

## Step 5: Namespaces in trackCfg.xml anpassen

Nur falls es eine Datei "trackCfg.xml" gibt.

Beispiel für den Anfang einer korrekten trackCfg.xml 
```
<?xml version="1.0" encoding="UTF-8" ?>
<trackCfg id="PI-Rail-Dev" version="16"
          xmlns="http://res.pirail.org/pi-rail-track.xsd"
          xmlns:pir="http://res.pirail.org/pi-rail.xsd">
```

1. Gehen Sie zurück zur index-Seite des Moduls
2. Klicken Sie hinter "trackCfg.xml" auf "edit" 
1. Ersetzen Sie in einer der ersten Zeilen der trackCfg.xml den Namespace (```xmlns=pi-rail-track-v1"```) durch ```xmlns="http://res.pirail.org/pi-rail.xsd"```
1. Ersetzen Sie in einer der ersten Zeilen der trackCfg.xml den PI-Rail Namespace (```xmlns:pir="pi-rail-v1"```) durch ```xmlns:pir="http://res.pirail.org/pi-rail-track.xsd"```
1. Klicken Sie auf "Save" --> es erschient OK und das Modul startet neu


## Step 6: Modul-Konfiguration

Dieser Schritt unterschiedet sich nicht von der Inbetriebnahme eines neu gekauften CTC-Moduls.
Das aktualisierte Modul trägt die ID "Leere Config".
