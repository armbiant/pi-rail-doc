---
layout: page
title:  "App Models"
tags: Software Models
---

## Models in App Memory

This how different models are related in App's memory:

![UML Artifacts Overview]({{site.baseurl}}/assets/images/UML-App-Models.png)

