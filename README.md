PI-Rail documentation project based on [Jekyll](http://jekyllrb.com)

Generated output can be found [here](https://pi-rail.gitlab.io/pi-rail-doc)

See [PI-Rail-FX](https://gitlab.com/pi-rail/pi-rail-fx) for documentation on desktop app and [PI-Rail-Arduino](https://gitlab.com/pi-rail/pi-rail-arduino) for documentation on firmware.

## Setup of Jekyll preview on Ubuntu
1. Follow instructions on https://jekyllrb.com/docs/installation/ubuntu/
2. Remove Gemfile.lock
3. Run "bundle install" in terminal
4. Now you can execute "./run.sh" in WebStorm or IntelliJ terminal